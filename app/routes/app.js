var express = require("express");
var path = require("path");
var router = express.Router();

router.get("/", async (req, res, next) => {
    res.render("index.pug");
});


module.exports = router;